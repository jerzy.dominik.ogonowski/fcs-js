'use strict';

import {FCS, FLAGS, FCSClient} from '../src';


//server

const fcs = new FCS();

fcs.setFlags({
    auth: {flag: FLAGS.ACTIVE},
    'my-carousel': {flag: FLAGS.COOKIE},
    'new-bonus': {
        flag: FLAGS.TAG,
        name: 'username',
        value: ['test123', 'test456'],
    },
});

fcs.setTag('env', 'production');
fcs.setTag('username', 'test123');

fcs.setCookies({
    'fcs-my-carousel': '1',
});

if (fcs.feature('my-carousel')) {
    console.log('my carousel is active on server');
}

// return features list and return to client
const features = fcs.getActiveFeatures();


// frontend

FCSClient.setActiveFeatures(features);

const cssClasses = FCSClient.getBodyCssClasses();

if (FCSClient.feature('my-carousel')) {
    console.log('my carousel is active on client');
}

const css = `
	body.fcs-my-carousel p {
		color: red;
	}
`;

const html = `<html>
	<head>
		<title>Test</title>
	</head>
	<style type="text/css">${css}</style>
	<body class="${cssClasses}">
		<p>OK</p>
	</body>
</html>`;
