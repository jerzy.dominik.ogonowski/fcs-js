'use strict';

import express from 'express';
import {FCS, FLAGS, middlewares} from '../src';

const {flags, tag, access, feature} = middlewares;

const app = express();

// flags() middleware accepts either direct value or callback with no arguments
// it defines list of flags and also attaches fcs object to request object
app.use(
    flags(() => {
        // with callback you can load flags from file or cache here
        return {
            'my-feature': {
                flag: FLAGS.TAG,
                name: 'domain',
                value: 'domain.com',
            },
            'my-feature2': {
                flag: FLAGS.TAG,
                name: 'env',
                value: 'development',
            },
        };
    }, 60 * 1000) // set timeout to cache list of flags for 60 sec
);

// tag() middleware accepts either direct value or callback with req argument
app.use(tag('domain', req => req.get('origin')));
app.use(tag('env', 'development'));

// access() middleware disables access to entire route
app.post(
    '/new-feature',
    access('my-feature'),
    (req, res, next) => {
        res.send(
            'Route only available if feature is active'
        );
    },
    (err, req, res, nex) => {
        res.send('Feature is not active yet');
    }
);

// feature() middleware enables new code as conditional middleware
app.post(
    '/new-feature2',
    feature('my-feature2', (req, res, next) => {
        res.send(
            'Middleware only available if feature is active, otherwise middleware is skipped'
        );
    }),
    (req, res, next) => {
        res.send('Always ok');
    }
);

// fcs object is attached to request object which allows you to use feature flags inside your code
app.post(
    '/new-feature3',
    (req, res, next) => {
        const {fcs} = req;
        if (fcs.feature('my-feature-3')) {
            res.send('New response under development');
        } else {
            res.send('Old response');
        }
    }
);
