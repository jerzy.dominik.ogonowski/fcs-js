import { FCS } from './FCS';
import SpyInstance = jest.SpyInstance;
import { IFlag } from './interfaces';

let fcs: FCS;
beforeEach(() => {
    fcs = new FCS();
});

describe('Test flags', () => {
    describe('FLAG_INACTIVE', () => {
        test('Always return false', () => {
            fcs.setFlags({
                'my-feature': {flag: FCS.FLAG_INACTIVE},
            });
            expect(fcs.feature('my-feature')).toBe(false);
        });
    });
    describe('FLAG_ACTIVE', () => {
        test('Always return true', () => {
            fcs.setFlags({
                'my-feature': {flag: FCS.FLAG_ACTIVE},
            });
            expect(fcs.feature('my-feature')).toBe(true);
        });
    });
    describe('FLAG_COOKIE', () => {
        test('Return false if cookie is not defined', () => {
            fcs.setFlags({
                'my-feature': {flag: FCS.FLAG_COOKIE},
            });
            fcs.setCookies({});
            expect(fcs.feature('my-feature')).toBe(false);
        });
        test('Return true if cookie is defined', () => {
            fcs.setFlags({
                'my-feature': {flag: FCS.FLAG_COOKIE},
            });
            fcs.setCookies({
                'fcs-my-feature': 1,
            });
            expect(fcs.feature('my-feature')).toBe(true);
        });
        test('Accept only cookies with double dash instead of slash', () => {
            fcs.setFlags({
                'test/my-feature': {flag: FCS.FLAG_COOKIE},
                'test/my-feature2': {flag: FCS.FLAG_COOKIE},
            });
            fcs.setCookies({
                'fcs-test/my-feature': 1,
                'fcs-test--my-feature2': 1,
            });
            expect(fcs.feature('test/my-feature')).toBe(false);
            expect(fcs.feature('test/my-feature2')).toBe(true);
        });
    });
    describe('FLAG_TAG', () => {
        test('Return false if tag is not defined', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_TAG,
                    name: 'country',
                    value: 'UK',
                },
            });
            expect(fcs.feature('my-feature')).toBe(false);
        });
        test('Return false if tag is defined with different value', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_TAG,
                    name: 'country',
                    value: 'UK',
                },
            });
            fcs.setTag('country', 'DE');
            expect(fcs.feature('my-feature')).toBe(false);
        });
        test('Return true if tag is defined with the same value', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_TAG,
                    name: 'country',
                    value: 'UK',
                },
            });
            fcs.setTag('country', 'UK');
            expect(fcs.feature('my-feature')).toBe(true);
        });
        test('Return false if tag is not defined in array of options', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_TAG,
                    name: 'country',
                    value: ['UK', 'DE'],
                },
            });
            fcs.setTag('country', 'FR');
            expect(fcs.feature('my-feature')).toBe(false);
        });
        test('Return true if tag is defined in array of options', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_TAG,
                    name: 'country',
                    value: ['UK', 'DE'],
                },
            });
            fcs.setTag('country', 'UK');
            expect(fcs.feature('my-feature')).toBe(true);
        });
    });
    describe('FLAG_MULTI_AND', () => {
        test('Return false if definition of sub-flags is missing', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_MULTI_AND,
                },
            });
            expect(fcs.feature('my-feature')).toBe(false);
        });
        test('Return true if both sub-conditions are met', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_MULTI_AND,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.com',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'en'},
                    ],
                },
            });
            fcs.setTag('domain', 'domain.com');
            fcs.setTag('lang', 'en');
            expect(fcs.feature('my-feature')).toBe(true);
        });
        test('Return false if any of sub-conditions is not met', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_MULTI_AND,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.de',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'de'},
                    ],
                },
                'my-feature2': {
                    flag: FCS.FLAG_MULTI_AND,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.com',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'en'},
                    ],
                },
                'my-feature3': {
                    flag: FCS.FLAG_MULTI_AND,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.fr',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'fr'},
                    ],
                },
            });
            fcs.setTag('domain', 'domain.de');
            fcs.setTag('lang', 'en');
            expect(fcs.feature('my-feature')).toBe(false);
            expect(fcs.feature('my-feature2')).toBe(false);
            expect(fcs.feature('my-feature3')).toBe(false);
        });
    });
    describe('FLAG_MULTI_OR', () => {
        test('Return false if definition of sub-flags is missing', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_MULTI_OR,
                },
            });
            expect(fcs.feature('my-feature')).toBe(false);
        });
        test('Return true if ay of sub-conditions is met', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_MULTI_OR,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.de',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'de'},
                    ],
                },
                'my-feature2': {
                    flag: FCS.FLAG_MULTI_OR,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.de',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'en'},
                    ],
                },
                'my-feature3': {
                    flag: FCS.FLAG_MULTI_OR,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.com',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'de'},
                    ],
                },
            });
            fcs.setTag('domain', 'domain.de');
            fcs.setTag('lang', 'de');
            expect(fcs.feature('my-feature')).toBe(true);
            expect(fcs.feature('my-feature2')).toBe(true);
            expect(fcs.feature('my-feature3')).toBe(true);
        });
        test('Return false if none of sub-conditions is met', () => {
            fcs.setFlags({
                'my-feature': {
                    flag: FCS.FLAG_MULTI_OR,
                    flags: [
                        {
                            flag: FCS.FLAG_TAG,
                            name: 'domain',
                            value: 'domain.com',
                        },
                        {flag: FCS.FLAG_TAG, name: 'lang', value: 'en'},
                    ],
                },
            });
            fcs.setTag('domain', 'domain.de');
            fcs.setTag('lang', 'de');
            expect(fcs.feature('my-feature')).toBe(false);
        });
    });
    describe('Missing/invalid', () => {
        test('Return false if definition of flags is not defined', () => {
            fcs.setFlags({});
            expect(fcs.feature('my-feature')).toBe(false);
        });
        test('Return false if definition of flags is invalid', () => {
            fcs.setFlags({
                // @ts-ignore
                'my-feature': {},
                // @ts-ignore
                'my-feature2': {flag: 'test.js.ts.js'},
            });
            expect(fcs.feature('my-feature')).toBe(false);
            expect(fcs.feature('my-feature2')).toBe(false);
        });
    });
});

describe('Cache', () => {
    let spy: SpyInstance<boolean, [string, IFlag]>;
    beforeEach(() => {
        spy = jest.spyOn(fcs, 'isFlagActive');
    });
    afterEach(() => {
        spy.mockRestore();
    });
    test('Ensure that result of feature assessment is not cached when tag is updated', () => {
        fcs.setFlags({
            'my-feature': {
                flag: FCS.FLAG_TAG,
                name: 'country',
                value: 'UK',
            },
        });
        fcs.setTag('country', 'UK');
        expect(fcs.feature('my-feature')).toBe(true);
        fcs.setTag('country', 'DE');
        expect(fcs.feature('my-feature')).toBe(false);
    });
    test('Ensure that result of feature assessment is not cached anymore when clearCache is used', () => {
        fcs.setFlags({
            'my-feature': {
                flag: FCS.FLAG_TAG,
                name: 'country',
                value: 'UK',
            },
        });
        fcs.setTag('country', 'UK');
        fcs.feature('my-feature');
        expect(spy).toHaveBeenCalledTimes(1);
        fcs.feature('my-feature');
        expect(spy).toHaveBeenCalledTimes(1);
        fcs.clearCache();
        fcs.feature('my-feature');
        expect(spy).toHaveBeenCalledTimes(2);
    });
});

describe('getActiveFeatures', () => {
    test('Expect to return only features that meet conditions of flags', () => {
        fcs.setFlags({
            'my-feature': {flag: FCS.FLAG_ACTIVE},
            'my-feature2': {flag: FCS.FLAG_INACTIVE},
            'my-feature3': {
                flag: FCS.FLAG_TAG,
                name: 'domain',
                value: 'domain.com',
            },
            'my-feature4': {
                flag: FCS.FLAG_TAG,
                name: 'lang',
                value: 'fr',
            },
        });
        fcs.setTag('domain', 'domain.com');
        fcs.setTag('lang', 'en');
        const activeFeatures = fcs.getActiveFeatures();
        expect(activeFeatures).toEqual(['my-feature', 'my-feature3']);
    });
});

describe('getBodyCssClasses', () => {
    test('Expect to return only CSS classes of features that meet conditions of flags', () => {
        fcs.setFlags({
            'my-feature': {flag: FCS.FLAG_ACTIVE},
            'my-feature2': {flag: FCS.FLAG_INACTIVE},
            'my-feature3': {
                flag: FCS.FLAG_TAG,
                name: 'domain',
                value: 'domain.com',
            },
            'my-feature4': {
                flag: FCS.FLAG_TAG,
                name: 'lang',
                value: 'fr',
            },
        });
        fcs.setTag('domain', 'domain.com');
        fcs.setTag('lang', 'en');
        const cssClasses = fcs.getBodyCssClasses();
        expect(cssClasses).toEqual('fcs-my-feature fcs-my-feature3');
    });
    test('Expect to automatically turn slashes into double dashes in names of returned CSS classes', () => {
        fcs.setFlags({
            'test/my-feature': {flag: FCS.FLAG_ACTIVE},
        });
        const cssClasses = fcs.getBodyCssClasses();
        expect(cssClasses).toEqual('fcs-test--my-feature');
    });
});

describe('turnSlashesIntoDoubleDashes', () => {
    test('Expect to turn all the slashes in the provided string into double dashes', () => {
        expect(fcs.turnSlashesIntoDoubleDashes('a/b/c/d')).toEqual(
            'a--b--c--d'
        );
    });
});
