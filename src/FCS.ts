import { ICookies, IFlag, IFlags, TTagValue } from './interfaces';
import { FLAGS } from './enums';

export class FCS {
    /**
     * Constants of flags and their integers
     */
    public static FLAG_INACTIVE = FLAGS.INACTIVE;
    public static FLAG_ACTIVE = FLAGS.ACTIVE;
    public static FLAG_COOKIE = FLAGS.COOKIE;
    public static FLAG_TAG = FLAGS.TAG;
    public static FLAG_MULTI_AND = FLAGS.MULTI_AND;
    public static FLAG_MULTI_OR = FLAGS.MULTI_OR;

    /**
     * Variable with configuration data
     */
    private config = {
        tags: {
            env: 'staging',
            username: '',
            ip: '0.0.0.0',
            role: '',
        },
        cookies: {},
        flags: {},
    };

    /**
     * In-memory cache with results of resolving flags for features
     */
    private cache = {};

    /**
     * Clear in-memory cache
     */
    public clearCache(): void {
        this.cache = {};
    }

    /**
     * Turn slashes into double dashes
     * @param name
     */
    public turnSlashesIntoDoubleDashes(name: string): string {
        return name.replace(/\//g, '--');
    }

    /**
     * Sets tag's name and value
     * @param name
     * @param value
     */
    public setTag(name: string, value: TTagValue): void {
        this.config.tags[name] = value;
        this.clearCache();
    }

    /**
     * Set list of features with details of flags
     * @param flags
     */
    public setFlags(flags: IFlags): void {
        this.config.flags = flags;
        this.clearCache();
    }

    /**
     * Set cookies
     * @param cookies
     */
    public setCookies(cookies: ICookies): void {
        this.config.cookies = cookies;
        this.clearCache();
    }

    /**
     * Returns list of active features
     */
    public getActiveFeatures(): string[] {
        const activeFeatures = [];
        for (let name in this.config.flags) {
            if (this.feature(name)) {
                activeFeatures.push(name);
            }
        }
        return activeFeatures;
    }

    /**
     * Returns list of CSS classes for frontend for <body> element
     * @param features
     */
    public getBodyCssClasses(
        features: string[] = this.getActiveFeatures()
    ): string {
        let bodyClasses = '';
        for (let feature of features) {
            bodyClasses +=
                (bodyClasses ? ' ' : '') +
                'fcs-' +
                this.turnSlashesIntoDoubleDashes(feature);
        }
        return bodyClasses;
    }

    /**
     * Checks if feature is active
     * @param name
     */
    public feature(name: string): boolean {
        if (this.cache[name] !== undefined) {
            return this.cache[name];
        } else {
            const flag = this.config.flags[name];
            if (flag) {
                return (this.cache[name] = this.isFlagActive(name, flag));
            } else {
                return false;
            }
        }
    }

    /**
     * Validates flag details to check if feature is active
     * @param name
     * @param flag
     */
    public isFlagActive(name: string, flag: IFlag): boolean {
        if (flag && flag.flag !== undefined) {
            switch (flag.flag) {
                case FCS.FLAG_INACTIVE:
                    return false;

                case FCS.FLAG_ACTIVE:
                    return true;

                case FCS.FLAG_COOKIE:
                    return Boolean(
                        this.config.cookies[
                        'fcs-' + this.turnSlashesIntoDoubleDashes(name)
                            ]
                    );

                case FCS.FLAG_TAG:
                    return (
                        flag.name !== undefined &&
                        flag.value !== undefined &&
                        this.config.tags[flag.name] !== undefined &&
                        (Array.isArray(flag.value)
                            ? flag.value.indexOf(
                            this.config.tags[flag.name]
                        ) !== -1
                            : this.config.tags[flag.name] === flag.value)
                    );

                case FCS.FLAG_MULTI_AND:
                    if (flag.flags && flag.flags.length) {
                        for (let sub_flag of flag.flags) {
                            if (!this.isFlagActive(name, sub_flag)) {
                                return false;
                            }
                        }
                        return true;
                    }
                    return false;

                case FCS.FLAG_MULTI_OR:
                    if (flag.flags && flag.flags.length) {
                        for (let sub_flag of flag.flags) {
                            if (this.isFlagActive(name, sub_flag)) {
                                return true;
                            }
                        }
                    }
                    return false;
            }
        }
        return false;
    }
}
