import FCSClient from './FCSClient';

describe('getActiveFeatures/setActiveFeatures', () => {
    test('Expect to return with getter the same list of features as defined with setter', () => {
        const features = ['my-feature', 'my-feature2', 'my-feature3'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.getActiveFeatures()).toEqual(features);
    });
});

describe('getBodyCssClasses', () => {
    test('Expect to return list of css classes from list defined with setter', () => {
        const features = ['my-feature', 'my-feature2', 'my-feature3'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.getBodyCssClasses()).toEqual(
            'fcs-my-feature fcs-my-feature2 fcs-my-feature3'
        );
    });
    test('Expect to return list of css classes from argument', () => {
        const features = ['my-feature', 'my-feature2'];
        expect(FCSClient.getBodyCssClasses(features)).toEqual(
            'fcs-my-feature fcs-my-feature2'
        );
    });
    test('Expect to automatically turn slashes into double dashes in names of returned CSS classes', () => {
        const features = ['test/my-feature', 'test/my-feature2'];
        expect(FCSClient.getBodyCssClasses(features)).toEqual(
            'fcs-test--my-feature fcs-test--my-feature2'
        );
    });
});

describe('feature', () => {
    test('Expect to return true if feature is present among active features defined with setter', () => {
        const features = ['my-feature', 'my-feature2', 'my-feature3'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.feature('my-feature')).toBe(true);
    });
    test('Expect to return false if feature is not present among active features defined with setter', () => {
        const features = ['my-feature'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.feature('my-feature2')).toBe(false);
    });
});

describe('multiFeature', () => {
    test('Expect to return true if feature (string or array) is present among active features defined with setter', () => {
        const features = ['my-feature', 'my-feature2', 'my-feature3'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.multiFeature('my-feature')).toBe(true);
        expect(FCSClient.multiFeature(['my-feature'])).toBe(true);
    });
    test('Expect to return false if feature (string or array) is not present among active features defined with setter', () => {
        const features = ['my-feature'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.multiFeature('my-feature2')).toBe(false);
        expect(FCSClient.multiFeature(['my-feature2'])).toBe(false);
    });
    test('Expect to return true if any of listed features (comma-separated string or array) is present among active features defined with setter', () => {
        const features = ['my-feature'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.multiFeature('my-feature,my-feature2')).toBe(true);
        expect(FCSClient.multiFeature(['my-feature', 'my-feature2'])).toBe(
            true
        );
    });
    test('Expect to return false if none of listed features (comma-separated string or array) is present among active features defined with setter', () => {
        const features = ['my-feature3'];
        FCSClient.setActiveFeatures(features);
        expect(FCSClient.multiFeature('my-feature,my-feature2')).toBe(false);
        expect(FCSClient.multiFeature(['my-feature', 'my-feature2'])).toBe(
            false
        );
    });
});

describe('turnSlashesIntoDoubleDashes', () => {
    test('Expect to turn all the slashes in the provided string into double dashes', () => {
        expect(FCSClient.turnSlashesIntoDoubleDashes('a/b/c/d')).toEqual(
            'a--b--c--d'
        );
    });
});
