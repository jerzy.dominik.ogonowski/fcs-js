export class FCSClient {
    /**
     * Variable with array of active features
     */
    private activeFeatures: string[] = [];

    /**
     * Turn slashes into double dashes
     * @param name
     */
    public turnSlashesIntoDoubleDashes(name: string): string {
        return name.replace(/\//g, '--');
    }

    /**
     * Set list of active features
     * @param features
     */
    public setActiveFeatures(features: string[]): void {
        this.activeFeatures = features;
    }

    /**
     * Get list of active features
     */
    public getActiveFeatures(): string[] {
        return this.activeFeatures;
    }

    /**
     * Returns list of CSS classes for frontend for <body> element
     * @param features
     */
    public getBodyCssClasses(
        features: string[] = this.getActiveFeatures()
    ): string {
        let bodyClasses = '';
        for (let feature of features) {
            bodyClasses +=
                (bodyClasses ? ' ' : '') +
                'fcs-' +
                this.turnSlashesIntoDoubleDashes(feature);
        }
        return bodyClasses;
    }

    /**
     * Checks if feature is active
     * @param name
     */
    public feature(name: string): boolean {
        return this.getActiveFeatures().indexOf(name) !== -1;
    }

    /**
     * Checks if any of multiple features is active
     * @param name
     */
    public multiFeature(name: string | string[]): boolean {
        const names: string[] | boolean = Array.isArray(name)
            ? name
            : name.indexOf(',') !== -1
                ? name.split(',')
                : false;
        if (!names && typeof name === 'string') {
            return this.feature(name);
        } else {
            for (let sub_name of names) {
                if (this.feature(sub_name)) {
                    return true;
                }
            }
            return false;
        }
    }
}

export default new FCSClient();
