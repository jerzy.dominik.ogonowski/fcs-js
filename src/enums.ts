export enum FLAGS {
    INACTIVE = 0,
    ACTIVE = 1,
    COOKIE = 2,
    TAG = 3,
    MULTI_AND = 4,
    MULTI_OR = 5,
}
