import FCSClient from './FCSClient';
import { FCS } from './FCS';
import {
    flags,
    tag,
    access,
    feature,
    IRequestWithFcs,
} from './middlewares';
import { FLAGS } from './enums';
import {
    ICookies,
    TFlagValue,
    TTagValue,
    IFlag,
    IFlags,
    TTagCreator,
    TFlagsCreator,
} from './interfaces';

const middlewares = {
    flags,
    tag,
    access,
    feature,
};

export {
    FCS,
    FCSClient,
    middlewares,
    FLAGS,
    ICookies,
    TFlagValue,
    TTagValue,
    IFlag,
    IFlags,
    IRequestWithFcs,
    TTagCreator,
    TFlagsCreator,
};
