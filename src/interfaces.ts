import { FLAGS } from './enums';

export interface ICookies {
    [name: string]: string | number | boolean;
}

export type TFlagValue = string | string[] | number | number[] | boolean;

export type TTagValue = string | number | boolean;

export interface IFlag {
    flag: FLAGS;
    name?: string;
    value?: TFlagValue;
    flags?: IFlag[];
}

export interface IFlags {
    [name: string]: IFlag;
}

export type TTagCreator = (req: any) => string;

export type TFlagsCreator = () => IFlags;
