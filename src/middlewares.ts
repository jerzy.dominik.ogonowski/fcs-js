import memoize from 'memoizee';

import { FCS } from './FCS';
import { IFlags, TFlagsCreator, TTagCreator, TTagValue } from './interfaces';

export interface IRequestWithFcs {
    fcs: FCS;
}

export const flags = (value: IFlags | TFlagsCreator, maxAge = 60 * 1000) => {
    if (typeof value === 'function' && maxAge) {
        value = memoize(value, {maxAge});
    }
    return async (req: any, res: any, next: any) => {
        try {
            const fcs = new FCS();
            fcs.setFlags(typeof value === 'function' ? await value() : value);
            req.fcs = fcs;
            next();
        } catch (err) {
            next(err);
        }
    };
};

export const tag = (name: string, value: TTagValue | TTagCreator) => {
    return async (req: any, res: any, next: any) => {
        try {
            const {fcs} = req;
            fcs.setTag(name, typeof value === 'function' ? await value(req) : value);
            next();
        } catch (err) {
            next(err);
        }
    };
};

export const access = (name: string) => {
    return (req: any, res: any, next: any) => {
        try {
            const {fcs} = req;
            if (fcs.feature(name)) {
                next();
            } else {
                next(new Error('Permission denied'));
            }
        } catch (err) {
            next(err);
        }
    };
};

export const feature = (name: string, cb: Function) => {
    return async (req: any, res: any, next: any) => {
        try {
            const {fcs} = req;
            if (fcs.feature(name)) {
                await cb(req, res, next);
            } else {
                next();
            }
        } catch (err) {
            next(err);
        }
    };
};
